/*
 * OpenShift Event Stream Analytics
 *
 * Copyright © 2017 Red Hat, Inc.
 *
 * This file is part of OpenShift Event Stream Analytics.
 *
 * OpenShift Event Stream Analytics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenShift Event Stream Analytics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenShift Event Stream Analytics. If not, see <http ://www.gnu.org/licenses/>.
 */

 package com.redhat.sysdeseng;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.transforms.Values;
import org.apache.beam.sdk.io.kafka.KafkaIO;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;
import java.util.List;
import org.apache.beam.runners.flink.FlinkRunner;

/**
 * A ...
 * 
 */
public class OpenShiftAnalytics {
        public static final String TOKENIZER_PATTERN = "[^\\p{L}]+";
        private static final Logger LOG = LoggerFactory.getLogger(OpenShiftAnalytics.class);

        static final List<String> LINES = Arrays.asList("To be, or not to be: that is the question: ",
                        "Whether 'tis nobler in the mind to suffer ", "The slings and arrows of outrageous fortune, ",
                        "Or to take arms against a sea of troubles, ");

        public static void main(String[] args) throws Exception {
                PipelineOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
                                .as(PipelineOptions.class);
                options.setRunner(FlinkRunner.class);

                Pipeline pipeline = Pipeline.create(options);

                KafkaIO.Read<String, String> reader = KafkaIO.<String, String>read()
                                .withBootstrapServers(options.getBootstrapServers()).withTopic(options.getTopics())
                                .withKeyDeserializer(StringDeserializer.class)
                                .withValueDeserializer(StringDeserializer.class);

                PCollection<String> streamData = pipeline.apply(reader.withoutMetadata())
                                .apply(Values.<String>create());

                PCollection<String> words = streamData.apply(ParDo.of(new Uppercase()));

                words.apply(KafkaIO.<String, String>write().withBootstrapServers("localhost:9092")
                                .withTopic("beam.results").withValueSerializer(StringSerializer.class)
                                .withKeySerializer(StringSerializer.class).values());

                pipeline.run();

        }
}
